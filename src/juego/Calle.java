package juego;

import java.awt.Color;

import entorno.Entorno;

public class Calle {
	private int x;
	private double y;
	private int alto;
	private int ancho;
	
	
	public Calle(int x, double y, int alto, int ancho) {
		this.x=x;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;
	}
	public int getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	
	public int getAlto() {
		return this.alto;
	}
	
	public int getAncho() {
		return this.ancho;
	}
	public void setX(int x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}
	public void bajando() {
		this.y=this.y+0.5;
		if(this.y>901) {
			this.setY(0);
		}
	}
	public void dibujarse(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.RED);
	}
}
